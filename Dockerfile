FROM node:latest

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package.json /usr/src/app/
RUN npm install

COPY . /usr/src/app
RUN apt-get update && apt-get install make gcc g++ python bash && cd yolo && make

ENV PORT 5000
EXPOSE $PORT
CMD [ "npm", "start" ]
