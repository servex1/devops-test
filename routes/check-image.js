// var express = require('express');
// var router = express.Router();


// /* GET home page. */
// router.get('/', function(req, res, next) {
//   // res.render('index', { title: 'Express' });
//   dir = exec("cd yolo && ./darknet detect cfg/yolov3.cfg yolov3.weights data/dog.jpg", function(err, stdout, stderr) {
//     if (err) {
//       res.send(stderr);
//     } else {
//       res.send(stdout);
//     }
//   });
// });

// module.exports = router;

var express = require('express');
var router = express.Router();
var sys = require('sys')
var exec = require('child_process').exec;
 
//multer object creation
var multer  = require('multer')
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
  }
})
 
var upload = multer({ storage: storage })
 
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
 
router.post('/', upload.single('imageupload'),function(req, res) {
  var fname = req.file.filename.split('.').slice(0, -1).join('.');
  dir = exec(`cd yolo && ./darknet detect cfg/yolov3.cfg yolov3.weights ../public/uploads/${req.file.filename} -out ../public/results/${fname}`, function(err, stdout, stderr) {
        if (err) {
          res.send(stderr);
        } else {
          // res.send(stdout);
          res.writeHead(
            301,
            {Location: '/results/'+req.file.filename}
          );
          res.end();
        }
      });
});
 
module.exports = router;